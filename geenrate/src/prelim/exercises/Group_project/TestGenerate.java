package prelim.exercises.Group_project;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TestGenerate {
    //Generate names
    public static void main(String[] args)  throws FileNotFoundException {
        Scanner kbd = new Scanner(System.in);
        //count of names to
        System.out.println("Enter count of names to generate and sort: ");
        int count = kbd.nextInt();
        //
        List<String> array = getName(count);
//        //methods to sort
//        ins_sort(array);
        bub_sort(array);
//        sel_sort(array);
    }

    public static List<String> getName(int count) throws FileNotFoundException {
            File l = new File("namesList/lastNames.txt");
            File f = new File("namesList/firstNames.txt");
            List<String> lines = new ArrayList<>();
            generateName gName = new generateName();
            for (int i = 0; i < count; ++i) {
                String lastName = gName.fetch(l);
                String firstName = gName.fetch(f);
                if (!lines.contains(lastName) && !lines.contains(firstName))
                    lines.add(firstName + " " + lastName);
            }
        return lines;
    }
    private static void ins_sort(List <String> lines) {
        //
        //Insertion Sort
        InsertionSort IS = new InsertionSort();
        String[] array3 = new String[lines.size()];
        lines.toArray(array3);
        IS.sortArray(array3);
        IS.showElements(array3);
    }

    private static void bub_sort(List <String> lines) {
        //Bubble Sort
        BubbleSort BS = new BubbleSort();
        String[] array2 = new String[lines.size()];
        lines.toArray(array2);
        BS.sortArray(array2);
        BS.showElements(array2);
    }

    private static void sel_sort(List <String> lines) {
        //Selection Sort
        SelectionSort SS = new SelectionSort();
        String[] array = new String[lines.size()];
        lines.toArray(array);
        SS.sortArray(array);
        SS.showElements(array);
    }
    }

