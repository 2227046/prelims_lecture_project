/**
 * Sorts an array of String using the
 * Bubble Sort Algorithm:
 * 1. TO DO
 */
package prelim.exercises.Group_project;
public class BubbleSort {
    public static void sortArray(String[] array) {
        int n = array.length; // 1
        for (int y = 0; y < n - 1; y++) { //
            String temp;
            for (int x = y + 1; x < n; x++) {
                if (array[y].compareToIgnoreCase(array[x]) == 0) { // if equal then it is average case, if greater than 0
//                    Then it is the best case and if it is lesser than 0 then it is worst case.
                    temp = array[y];
                    array[y] = array[x];
                    array[x] = temp;
                }
            }
        }
    }// end of sortArray method
    public static void showElements(String[] array) {
        for (int x = 0; x < array.length; x++)
            System.out.printf("%10s\n", array[x]);
    }// end of showElements method
}// end of BubbleSort class
