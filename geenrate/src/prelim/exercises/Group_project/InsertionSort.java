/**
 * Sorts an array of String using the
 * Insertion Sort Algorithm:
 * 1. TO DO
 */
package prelim.exercises.Group_project;
public class InsertionSort {
    public static void sortArray(String[] array) {
        for (int y = 1; y < array.length; y++) {
            String temp = array[y];
            int x = y - 1;
            while (x >= 0) {
                if (temp.compareTo(array[x]) > 0) { //// if equal then it is average case, if greater than 0
//                    Then it is the best case and if it is lesser than 0 then it is worst case.
                    break;
                }
                array[x + 1] = array[x];
                x--;
            }
            array[x + 1] = temp;
        }
    }// end of sortArray method
    public static void showElements(String[] array) {
        for (int x = 0; x < array.length; x++)
            System.out.printf("%10s\n", array[x]);
    }// end of showElements method
}// end of InsertionSort class
