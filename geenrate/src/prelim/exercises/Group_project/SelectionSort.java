/**
 * Sorts an array of String using the
 * Selection Sort Algorithm:
 * 1. TO DO
 */
package prelim.exercises.Group_project;
public class SelectionSort {
    public static void sortArray(String[] array) {
        int n = array.length;
        for (int x = 0; x < n - 1; x++) {
            int min_index = x;
            String temp = array[x];
            for (int y = x + 1; y < n; y++) {
                if (array[y].compareToIgnoreCase(temp) == 0) { // if equal then it is average case, if greater than 0
//                    Then it is the worst case and if it is lesser than 0 then it is the best case.

                    temp = array[y];
                    min_index = y;
                }
                String a = array[min_index];
                while (min_index > x) {
                    array[min_index] = array[min_index - 1];
                    min_index--;
                }
                array[x] = a;
            }
        }
    } // end of sortArray method
    public static void showElements(String[] array) {
        for (int x = 0; x < array.length; x++)
            System.out.printf("%10s\n", array[x]);
    }// end of showElements method
}// end of SelectionSort class
